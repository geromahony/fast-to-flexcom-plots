# FAST to Flexcom Plots

A basic python script to convert [FAST](https://www.nrel.gov/wind/nwtc/fast.html) results to [Flexcom](https://www.woodplc.com/capabilities/digital-and-technology/software,-applications-and-analytics/flexcom) plot files.

FAST tabular output must be output in tab delimeted text format so the script can parse the output channels from the file(`OutFileFmt` : `1` and `TabDelim` : `True` in the \*.fst file). Each output channel is plotted against the `Time` channel. Flexcom timetrace plots(*.mplt) are produced with the `FAST.<channel name>.mplt` naming convention.



# Usage
The script has two command line flags:

`--input`  to specify an input file, can be a relative path, i.e `../<my_fast_out_file.out>`

`--channels` to specify FAST channel names to be converted into plots. If omitted, all channels in the *.out file are plotted

## Example call:

```python
python FASTtoFlexcomPlots.py --input <my_fast_out_file.out> --channels "RtSpeed RtAeroFxh RtAeroPwr NacYaw PtfmYaw PtfmPitch"
```
